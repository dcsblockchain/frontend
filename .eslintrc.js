module.exports = {
    "parser": "babel-eslint",
    "extends": "airbnb",
    "plugins": ["prettier"],
    "rules": {
        "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }],
        "prettier/prettier": "error",
        "arrow-parens": [0]
    }
};