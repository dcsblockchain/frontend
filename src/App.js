import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Home from './containers/home';
import Login from './containers/Login';
import Dashboard from './containers/Dashboard';
import './App.css';

const App = () => (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route path="/login" component={Login} />
    <Route path="/dashboard" component={Dashboard} />
  </Switch>
);

export default App;
