import React from 'react';
import { observer } from 'mobx-react';
import { Button, Icon } from 'semantic-ui-react';
import { handleItemClick } from './utils';
import './SidebarItem.css';

import DashboardStore from '../Dashboard/Dashboard.store';

const SidebarItem = observer(props => (
  <Button
    className={`sidebar-item ${
      DashboardStore.currentTab === props.title ? 'sidebar-item-selected' : ''
    }`}
    onClick={() => handleItemClick(props.title, props)}
  >
    <div className="sidebar-item-content">
      <span>{props.title}</span>
      <Icon disabled name={props.icon} />
    </div>
  </Button>
));

export default SidebarItem;
