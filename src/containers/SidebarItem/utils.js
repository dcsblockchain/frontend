import DashboardStore from '../Dashboard/Dashboard.store';

const handleItemClick = title => {
  DashboardStore.currentTab = title;
};

export { handleItemClick };
