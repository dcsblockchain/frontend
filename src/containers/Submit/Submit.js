import React from 'react';
import { observer } from 'mobx-react';
import { Button, Checkbox, Form } from 'semantic-ui-react';
import {
  handleChange,
  handleClear,
  handleDateChange,
  handleSubmit,
  handleToggle,
} from './utils';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import SubmitStore from './Submit.store';

import './Submit.css';
import 'react-datepicker/dist/react-datepicker-cssmodules.css';

const Submit = observer(() => (
  <div className="blockchain-container">
    <h1>New Transaction</h1>
    <Form
      id="new-transaction-form"
      className="form-container"
      onSubmit={this.handleSubmit}
    >
      <div className="form-row">
        <Form.Input
          label="Buyer ID"
          type="number"
          name="buyerId"
          onChange={handleChange}
        />
      </div>
      <div className="form-row">
        <Form.Input
          className="half-width left"
          label="Buyer First Name"
          type="text"
          name="buyerFirstName"
          onChange={handleChange}
        />
        <Form.Input
          className="half-width right"
          label="Buyer Last Name"
          type="text"
          name="buyerLastName"
          onChange={handleChange}
        />
      </div>
      <div className="form-row">
        <Form.Input
          className="half-width left"
          label="Seller First Name"
          type="text"
          name="sellerFirstName"
          onChange={handleChange}
        />
        <Form.Input
          className="half-width right"
          label="Seller Last Name"
          type="text"
          name="sellerLastName"
          onChange={handleChange}
        />
      </div>
      <div className="form-row">
        <Form.Input
          className="full-width"
          label="Address"
          type="text"
          name="address"
          onChange={handleChange}
        />
      </div>
      <div className="form-row">
        <Form.Input
          className="half-width left"
          label="ZIP Code"
          type="number"
          name="zipCode"
          onChange={handleChange}
        />
        <Form.Input
          className="half-width right"
          label="County"
          type="text"
          name="county"
          onChange={handleChange}
        />
      </div>
      <div className="form-row">
        <Form.Input
          label="PDF URL"
          type="text"
          name="pdfUrl"
          onChange={handleChange}
        />
      </div>
      <div className="form-row">
        <Form.Input
          className="half-width left"
          label="Sales Price"
          type="number"
          name="salesPrice"
          onChange={handleChange}
        />
        <Form.Input
          className="half-width right"
          label="Telephone Number"
          type="string"
          onChange={handleChange}
        />
      </div>
      <div className="form-row">
        <Form.Field className="half-width left">
          <label>Sale Contract Date</label>
          <DatePicker
            selected={
              SubmitStore.saleContractDate
                ? moment(SubmitStore.saleContractDate)
                : moment()
            }
            onChange={handleDateChange}
            name="saleContractDate"
            popperPlacement="auto"
          />
        </Form.Field>
      </div>
      <div className="form-row">
        <Form.Field>
          <Checkbox
            label="Mobile Home?"
            name="isMobileHome"
            checked={SubmitStore.isMobileHome}
            onChange={handleToggle}
          />
        </Form.Field>
      </div>
      {SubmitStore.isMobileHome && (
        <React.Fragment>
          <div className="form-row">
            <Form.Input
              className="half-width left"
              label="Make"
              type="text"
              name="make"
              onChange={handleChange}
            />
            <Form.Input
              className="half-width right"
              label="Model"
              type="text"
              name="model"
              onChange={handleChange}
            />
          </div>
          <div className="form-row">
            <Form.Input
              className="half-width left"
              label="Body Type"
              type="text"
              name="bodyType"
              onChange={handleChange}
            />
            <Form.Input
              className="half-width right"
              label="Color"
              type="text"
              name="color"
              onChange={handleChange}
            />
          </div>
          <div className="form-row">
            <Form.Input
              className="half-width left"
              label="Year"
              type="number"
              name="year"
              onChange={handleChange}
            />
          </div>
        </React.Fragment>
      )}
      <div className="form-row">
        <Button
          className="primary"
          content="Submit"
          onClick={handleSubmit}
          disabled={
            !SubmitStore.buyerId ||
            !SubmitStore.buyerFirstName ||
            !SubmitStore.buyerLastName
          }
        />
        <Button className="secondary" content="Clear" onClick={handleClear} />
      </div>
    </Form>
  </div>
));

export default Submit;
