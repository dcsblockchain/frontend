import SubmitStore from './Submit.store';

const handleChange = ({ target: { name, value, type } }) => {
  if (type === 'number') {
    SubmitStore[name] = +value;
  } else {
    SubmitStore[name] = value;
  }
};

const handleDateChange = e => (SubmitStore.saleContractDate = e.toDate());

const handleClear = () => {
  Object.keys(SubmitStore).forEach(
    key =>
      key !== 'isMobileHome'
        ? (SubmitStore[key] = null)
        : (SubmitStore.isMobileHome = false),
  );
  document.getElementById('new-transaction-form').reset();
};

const handleSubmit = () => {
  const postData = JSON.stringify(SubmitStore);
  console.log(postData);
  const url =
    'http://blockchain-load-balancer-1494972582.us-east-2.elb.amazonaws.com:8080/api/blockchain/insertnewtitletransfer';

  fetch(url, {
    headers: { 'content-type': 'application/json' },
    body: postData,
    method: 'POST',
  })
    .then(data => data.json())
    .then(res => console.log(res))
    .catch(error => console.log(error));
};

const handleToggle = () => {
  SubmitStore.isMobileHome = !SubmitStore.isMobileHome;
  if (!SubmitStore.isMobileHome) {
    SubmitStore.make = null;
    SubmitStore.model = null;
    SubmitStore.bodyType = null;
    SubmitStore.color = null;
    SubmitStore.year = null;
  }
};

export {
  handleChange,
  handleClear,
  handleDateChange,
  handleSubmit,
  handleToggle,
};
