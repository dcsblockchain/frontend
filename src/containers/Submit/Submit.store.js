import { observable, decorate } from 'mobx';

class Submit {
  buyerId = null;
  buyerFirstName = null;
  buyerLastName = null;
  sellerFirstName = null;
  sellerLastName = null;
  address = null;
  zipCode = null;
  county = null;
  pdfUrl = null;
  saleContractDate = null;
  salesPrice = null;
  telephoneNumber = null;
  isMobileHome = false;
  make = null;
  model = null;
  bodyType = null;
  color = null;
  year = null;
}

decorate(Submit, {
  buyerId: observable,
  buyerFirstName: observable,
  buyerLastName: observable,
  sellerFirstName: observable,
  sellerLastName: observable,
  address: observable,
  zipCode: observable,
  county: observable,
  pdfUrl: observable,
  saleContractDate: observable,
  salesPrice: observable,
  telephoneNumber: observable,
  isMobileHome: observable,
  make: observable,
  model: observable,
  bodyType: observable,
  color: observable,
  year: observable,
});

const SubmitStore = new Submit();
export default SubmitStore;
