import DashboardStore from './MyTransactions.store';

const handleItemClick = title => {
  DashboardStore.currentTab = title;
};

export { handleItemClick };
