import { observable, decorate } from 'mobx';

class MyTransactions {
  currentTab = 'Blockchain';
}
decorate(MyTransactions, {
  currentTab: observable,
});

const DashboardStore = new MyTransactions();
export default DashboardStore;
