import React from 'react';
import { observer } from 'mobx-react';
import { Image } from 'semantic-ui-react';

import SidebarItem from '../SidebarItem';

import GlobalStore from '../../GlobalStore.store';

import Logo from '../../assets/logo.png';
import './Sidebar.css';

const Sidebar = observer(() => (
  <div className="sidebar-container">
    <div className="sidebar-logo-container">
      <Image src={Logo} className="sidebar-logo" />
      <span>TChainz</span>
    </div>
    {GlobalStore.userType === 'User' && (
      <SidebarItem title="My Transactions" icon="database" />
    )}
    <SidebarItem title="Blockchain" icon="chain" />
    <SidebarItem title="Profile" icon="user" />
    <SidebarItem title="Submit" icon="upload" />
    <div style={{ width: '100%', marginTop: '650px' }}>
      <SidebarItem title="Log In" icon="arrow alternate circle right" />
    </div>
  </div>
));

export default Sidebar;
