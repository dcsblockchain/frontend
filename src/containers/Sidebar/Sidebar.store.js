import { observable, decorate } from 'mobx';

class Sidebar {
  currentTab = 'Blockchain';
}
decorate(Sidebar, {
  currentTab: observable,
});

const DashboardStore = new Sidebar();
export default DashboardStore;
