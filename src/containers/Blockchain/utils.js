import DashboardStore from './Blockchain.store';

const handleItemClick = title => {
  DashboardStore.currentTab = title;
};

export { handleItemClick };
