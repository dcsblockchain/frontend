import React from 'react';
import { observer } from 'mobx-react';
import { Button, Modal, Table } from 'semantic-ui-react';

import GlobalStore from '../../GlobalStore.store';

import './Blockchain.css';
import BlockchainStore from './Blockchain.store';

const Blockchain = observer(() => (
  <div className="blockchain-container">
    <h1>Recent Transactions</h1>
    <Table selectable>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Id</Table.HeaderCell>
          <Table.HeaderCell>Previous</Table.HeaderCell>
          <Table.HeaderCell>Next</Table.HeaderCell>
          <Table.HeaderCell>Address</Table.HeaderCell>
          <Table.HeaderCell>Date</Table.HeaderCell>
          <Table.HeaderCell>Title Company</Table.HeaderCell>
          <Table.HeaderCell />
          <Table.HeaderCell>Status</Table.HeaderCell>
        </Table.Row>
      </Table.Header>

      <Table.Body>
        {GlobalStore.nodes.map(item => (
          <Table.Row key={item.nodeId}>
            <Table.Cell
              onClick={() => {
                BlockchainStore.modalIsOpen = true;
                BlockchainStore.currentItem = item;
              }}
            >
              {item.nodeId}
            </Table.Cell>
            <Table.Cell
              onClick={() => {
                BlockchainStore.modalIsOpen = true;
                BlockchainStore.currentItem = item;
              }}
            >
              {item.previousOwner}
            </Table.Cell>
            <Table.Cell
              onClick={() => {
                BlockchainStore.modalIsOpen = true;
                BlockchainStore.currentItem = item;
              }}
            >
              {item.nextOwner}
            </Table.Cell>
            <Table.Cell
              onClick={() => {
                BlockchainStore.modalIsOpen = true;
                BlockchainStore.currentItem = item;
              }}
            >
              {item.address}
            </Table.Cell>
            <Table.Cell
              onClick={() => {
                BlockchainStore.modalIsOpen = true;
                BlockchainStore.currentItem = item;
              }}
            >
              {item.dateOfTransfer}
            </Table.Cell>
            <Table.Cell
              onClick={() => {
                BlockchainStore.modalIsOpen = true;
                BlockchainStore.currentItem = item;
              }}
            >
              {item.titleCompany}
            </Table.Cell>
            <Table.Cell>
              {item.status !== 'Rejected' && (
                <Button
                  onClick={() => {
                    BlockchainStore.modalIsChain = true;
                    BlockchainStore.modalIsOpen = true;
                    BlockchainStore.currentItem = item;
                  }}
                >
                  View Chain
                </Button>
              )}
            </Table.Cell>
            <Table.Cell
              onClick={() => {
                BlockchainStore.modalIsOpen = true;
                BlockchainStore.currentItem = item;
              }}
              negative={item.status === 'Rejected'}
              positive={item.status === 'Approved'}
              warning={item.status === 'Pending'}
            >
              {item.status}
            </Table.Cell>
          </Table.Row>
        ))}
      </Table.Body>
    </Table>
    <Modal
      open={BlockchainStore.modalIsOpen}
      onClose={() => {
        BlockchainStore.modalIsOpen = false;
        BlockchainStore.modalIsChain = false;
      }}
    >
      <Modal.Header>
        {`Information on Property ${BlockchainStore.currentItem.propertyId}`}
      </Modal.Header>
      <Modal.Content>
        {!BlockchainStore.modalIsChain && (
          <Modal.Description className="modal-content">
            <span>
              <b>Node Id: </b>
              {BlockchainStore.currentItem.nodeId}
            </span>
            <span>
              <b>Property Id: </b>
              {BlockchainStore.currentItem.propertyId}
            </span>
            <span>
              <b>Date of Transfer: </b>
              {BlockchainStore.currentItem.dateOfTransfer}
            </span>
            <span>
              <b>Previous Owner: </b>
              {BlockchainStore.currentItem.previousOwner}
            </span>
            <span>
              <b>Next Owner: </b>
              {BlockchainStore.currentItem.nextOwner}
            </span>
            <span>
              <b>Address: </b>
              {BlockchainStore.currentItem.address}
            </span>
            <span>
              <b>PDF Url: </b>
              {BlockchainStore.currentItem.pdfUrl}
            </span>
            <span>
              <b>Clerk: </b>
              {BlockchainStore.currentItem.clerk}
            </span>
            <span>
              <b>Status: </b>
              {BlockchainStore.currentItem.status}
            </span>
            <span>
              <b>Nonce: </b>
              {BlockchainStore.currentItem.nonce}
            </span>
          </Modal.Description>
        )}
        {BlockchainStore.modalIsChain && (
          <Modal.Description className="modal-content">
            {GlobalStore.nodes.map(node => {
              if (
                node.propertyId === BlockchainStore.currentItem.propertyId
                && node.status !== 'Rejected'
              ) {
                return (
                  <div>
                    <span>
                      <b>From: </b>
                      {node.previousOwner}
                    </span>
                    <span style={{ marginLeft: '5px' }}>
                      <b>To: </b>
                      {node.nextOwner}
                    </span>
                  </div>
                );
              }
              return null;
            })}
          </Modal.Description>
        )}
      </Modal.Content>
    </Modal>
  </div>
));

export default Blockchain;
