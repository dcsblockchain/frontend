import { observable, decorate } from 'mobx';

class Blockchain {
  currentItem = {};

  modalIsOpen = false;

  modalIsChain = false;
}
decorate(Blockchain, {
  currentItem: observable,
  modalIsOpen: observable,
  modalIsChain: observable,
});

const BlockchainStore = new Blockchain();
export default BlockchainStore;
