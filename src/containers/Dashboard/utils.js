import DashboardStore from './Dashboard.store';

const handleItemClick = title => {
  DashboardStore.currentTab = title;
};

export { handleItemClick };
