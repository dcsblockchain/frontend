import { observable, decorate } from 'mobx';

class Dashboard {
  currentTab = 'Blockchain';
}
decorate(Dashboard, {
  currentTab: observable,
});

const DashboardStore = new Dashboard();
export default DashboardStore;
