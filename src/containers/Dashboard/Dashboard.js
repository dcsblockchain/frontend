import React from 'react';
import { observer } from 'mobx-react';
import Sidebar from '../Sidebar';
import Blockchain from '../Blockchain';
import Profile from '../Profile';
import MyTransactions from '../MyTransactions';
import Submit from '../Submit';

import './Dashboard.css';

import DashboardStore from './Dashboard.store';

const Tabs = {
  Blockchain,
  MyTransactions,
  Profile,
  Submit,
};

const Dashboard = observer(
  class Dashboard extends React.Component {
    state = {
      current: '',
    };

    render() {
      const Tab = Tabs[DashboardStore.currentTab.replace(' ', '')];
      return (
        <div className="dashboard-container">
          <Sidebar />
          <div className="dashboard-content">
            <Tab />
          </div>
        </div>
      );
    }
  },
);

export default Dashboard;
