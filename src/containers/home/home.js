import React, { Component } from 'react';
import styles from './home.css';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 'home',
    };
  }

  render() {
    const { active } = this.state;
    return (
      <div>
        <div className={styles.appBar}>
          <div className={active === 'home' ? styles.menuItem_active : styles.menuItem_default}>Home</div>
          <div className={active === 'About' ? styles.menuItem_active : styles.menuItem_default}>About</div>
        </div>
        <div className={styles.contentContainer}>
          <div className={styles.heroContainer}>
            sample hero
          </div>
          <div className={styles.technicalContainer}>
            <div className={styles.technicalHeader}>
              <span className={styles.redLine} />
              <h2>Technical</h2>
              <span className={styles.redLine} />
            </div>
            <p className={styles.technicalLabel}>
              We Aim to make getting a house as easy as tapping a button.
            </p>
          </div>
          <div className={styles.aboutContainer}>
            sample about
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
