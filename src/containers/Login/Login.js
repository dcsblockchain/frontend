import React from 'react';
import { observer } from 'mobx-react';
import { Button, Card, Image, Input } from 'semantic-ui-react';
import Logo from '../../assets/logo.png';
import './Login.css';

import LoginStore from './Login.store';

const Login = observer(() => (
  <div className="login-container">
    <Card className="login-card">
      <Card.Content>
        <div className="login-card-content">
          <Image src={Logo} className="login-logo" />
        </div>
        <div className="login-form">
          <Input
            className="login-input"
            icon="user"
            iconPosition="left"
            placeholder="Username"
            value={LoginStore.username}
            onChange={e => {
              LoginStore.username = e.target.value;
            }}
          />
          <Input
            className="login-input"
            type="password"
            icon="lock"
            iconPosition="left"
            placeholder="Password"
            onChange={e => {
              LoginStore.password = e.target.value;
            }}
          />
          <Button className="login-button">Login</Button>
        </div>
      </Card.Content>
      <Card.Content extra>
        <span>
          Need to register?
          <a href="mailto:register@tchainz.com">
            <i>Email us</i>
          </a>
          .
        </span>
      </Card.Content>
    </Card>
  </div>
));

export default Login;
