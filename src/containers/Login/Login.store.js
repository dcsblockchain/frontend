import {
  observable,
  decorate,
} from 'mobx';

class Login {
  token = null;

  username = '';

  password = '';
}
decorate(Login, {
  token: observable,
  username: observable,
  password: observable,
});

const LoginStore = new Login();
export default LoginStore;
