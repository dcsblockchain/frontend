import React from 'react';
import { observer } from 'mobx-react';
import Sidebar from '../Sidebar';

import { handleItemClick } from './utils';
import './Profile.css';

import DashboardStore from './Profile.store';

const Profile = observer(() => (
  <div className="dashboard-container">
    <span>Profile</span>
  </div>
));

export default Profile;
