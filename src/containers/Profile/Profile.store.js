import { observable, decorate } from 'mobx';

class Profile {
  currentTab = 'Profile';
}
decorate(Profile, {
  currentTab: observable,
});

const DashboardStore = new Profile();
export default DashboardStore;
