import DashboardStore from './Profile.store';

const handleItemClick = title => {
  DashboardStore.currentTab = title;
};

export { handleItemClick };
