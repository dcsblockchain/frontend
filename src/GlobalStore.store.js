import { observable, decorate } from 'mobx';

const mockNodes = [
  {
    nodeId: 1,
    nonce: 9049650114,
    propertyId: 12345,
    dateOfTransfer: '9/10/2011',
    previousOwner: 'Alex Miller',
    nextOwner: 'Erkan Sen',
    address: '123 Fake Street Lane, Houston, TX, 77777',
    pdfUrl: 'https://countyclerk.com/12345/1',
    titleCompany: 'Titles by Kanye',
    clerk: 'Bob Clerkman',
    status: 'Approved',
  },
  {
    nodeId: 2,
    nonce: 8737446491,
    propertyId: 12345,
    dateOfTransfer: '8/10/2011',
    previousOwner: 'The Joker',
    nextOwner: 'Clark Kent',
    address: '123 Fake Street Lane, Houston, TX, 77777',
    pdfUrl: 'https://countyclerk.com/12345/2',
    titleCompany: 'Titles All Day',
    clerk: 'Bob Clerkman',
    status: 'Rejected',
  },
  {
    nodeId: 3,
    nonce: 7930968503,
    propertyId: 12345,
    dateOfTransfer: '9/10/2011',
    previousOwner: 'Erkan Sen',
    nextOwner: 'Clark Kent',
    address: '123 Fake Street Lane, Houston, TX, 77777',
    pdfUrl: 'https://countyclerk.com/12345/3',
    titleCompany: 'Titles All Day',
    clerk: 'Bob Clerkman',
    status: 'Approved',
  },
  {
    nodeId: 4,
    nonce: 1482931416,
    propertyId: 12346,
    dateOfTransfer: '9/29/2016',
    previousOwner: 'Oliver Queen',
    nextOwner: 'Bruce Wayne',
    address: 'Somewhere Lane, Houston, TX, 77007',
    pdfUrl: 'https://countyclerk.com/12346/1',
    titleCompany: 'We Love Titles',
    clerk: 'Slade Wilson',
    status: 'Pending',
  },
  {
    nodeId: 5,
    nonce: 4055066480,
    propertyId: 12347,
    dateOfTransfer: '10/29/2016',
    previousOwner: 'Diana Prince',
    nextOwner: 'Lois Lane',
    address: 'City Blvd, Houston, TX, 77007',
    pdfUrl: 'https://countyclerk.com/12347/1',
    titleCompany: 'We Love Titles',
    clerk: 'Slade Wilson',
    status: 'Approved',
  },
  {
    nodeId: 6,
    nonce: 6115472400,
    propertyId: 12347,
    dateOfTransfer: '11/05/2016',
    previousOwner: 'Lois Lane',
    nextOwner: 'Diana Prince',
    address: 'City Blvd, Houston, TX, 77007',
    pdfUrl: 'https://countyclerk.com/12347/2',
    titleCompany: 'Titles.org',
    clerk: 'Slade Wilson',
    status: 'Pending',
  },
];

class Global {
  nodes = mockNodes;

  userType = 'User';

  currentUser = {
    firstName: 'Slade',
    lastName: 'Wilson',
    name: 'Slade Wilson',
  };
}
decorate(Global, {
  nodes: observable,
});

const GlobalStore = new Global();
export default GlobalStore;
